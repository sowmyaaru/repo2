package com.NewTours.WebLocators;


public class NewToursHomepageLocators {
	
	
	public static final String RoundTrip_XPATH = "html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td[2]/b/font/input[1]";
	public static final String Oneway_XPATH = "html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td[2]/b/font/input[2]";
	public static final String Passengers_NAME = "passCount";
	public static final String Departingfrom_NAME = "fromPort";
	public static final String OnMonth_NAME = "fromMonth";
	public static final String OnDate_NAME = "fromDay";
	public static final String ArrivingIn_NAME = "toPort";
	public static final String ReturningMonth_NAME = "toMonth";
	public static final String ReturningDate_NAME = "toDay";
	public static final String EconomyClass_CSSSELECTOR = "input[value=Coach]";
	public static final String BusinessClass_CSSSELECTOR = "input[value=Business]";
	public static final String FirstClass_CSSSELECTOR = "input[value=First]";
	public static final String Airline_NAME = "airline";
	
	
	public static final String Continue_NAME = "findFlights";
	public static final String Continue1_NAME = "reserveFlights";
	public static final String FirstName_NAME = "passFirst0";
	public static final String LastName_NAME = "passLast0";
	public static final String Number_NAME = "creditnumber";
	public static final String Securepurchase_NAME = "buyFlights";
	public static final String CofirmMessage_XPATH = "html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr[1]/td[2]/table/tbody/tr[3]/td/p/font/b/font[2]";
	
	
	
}
